import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlenaryThreeComponent } from './plenary-three.component';

describe('PlenaryThreeComponent', () => {
  let component: PlenaryThreeComponent;
  let fixture: ComponentFixture<PlenaryThreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlenaryThreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlenaryThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
