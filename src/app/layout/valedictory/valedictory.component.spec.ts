import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValedictoryComponent } from './valedictory.component';

describe('ValedictoryComponent', () => {
  let component: ValedictoryComponent;
  let fixture: ComponentFixture<ValedictoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValedictoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValedictoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
