import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechThirtythreeComponent } from './tech-thirtythree.component';

describe('TechThirtythreeComponent', () => {
  let component: TechThirtythreeComponent;
  let fixture: ComponentFixture<TechThirtythreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechThirtythreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechThirtythreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
