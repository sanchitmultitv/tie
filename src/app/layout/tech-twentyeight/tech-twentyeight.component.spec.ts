import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechTwentyeightComponent } from './tech-twentyeight.component';

describe('TechTwentyeightComponent', () => {
  let component: TechTwentyeightComponent;
  let fixture: ComponentFixture<TechTwentyeightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechTwentyeightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechTwentyeightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
