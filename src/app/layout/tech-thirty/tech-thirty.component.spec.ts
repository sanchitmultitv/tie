import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechThirtyComponent } from './tech-thirty.component';

describe('TechThirtyComponent', () => {
  let component: TechThirtyComponent;
  let fixture: ComponentFixture<TechThirtyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechThirtyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechThirtyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
