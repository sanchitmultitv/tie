import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgendaTwoComponent } from './agenda-two.component';

describe('AgendaTwoComponent', () => {
  let component: AgendaTwoComponent;
  let fixture: ComponentFixture<AgendaTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgendaTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgendaTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
