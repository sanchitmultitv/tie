import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupChatTwoComponent } from './group-chat-two.component';

describe('GroupChatTwoComponent', () => {
  let component: GroupChatTwoComponent;
  let fixture: ComponentFixture<GroupChatTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupChatTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupChatTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
