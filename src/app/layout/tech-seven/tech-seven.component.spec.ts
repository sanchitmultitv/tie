import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechSevenComponent } from './tech-seven.component';

describe('TechSevenComponent', () => {
  let component: TechSevenComponent;
  let fixture: ComponentFixture<TechSevenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechSevenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechSevenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
