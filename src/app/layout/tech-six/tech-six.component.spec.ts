import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechSixComponent } from './tech-six.component';

describe('TechSixComponent', () => {
  let component: TechSixComponent;
  let fixture: ComponentFixture<TechSixComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechSixComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechSixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
