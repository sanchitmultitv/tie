import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechThirtyoneComponent } from './tech-thirtyone.component';

describe('TechThirtyoneComponent', () => {
  let component: TechThirtyoneComponent;
  let fixture: ComponentFixture<TechThirtyoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechThirtyoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechThirtyoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
