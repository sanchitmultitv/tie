import { Component, OnInit, OnDestroy } from '@angular/core';
import {FormControl} from '@angular/forms';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import {ToastrService} from 'ngx-toastr';
declare var $: any;
@Component({
  selector: 'app-ballroom-question',
  templateUrl: './ballroom-question.component.html',
  styleUrls: ['./ballroom-question.component.scss']
})
export class BallroomQuestionComponent implements OnInit,OnDestroy {
  textMessage = new FormControl('');
  msg;
  qaList;
  interval;
  constructor(private _fd: FetchDataService,private toastr:ToastrService) { }

  ngOnInit(): void {
   // this.getQA();
  //   this.interval = setInterval(() => {
  //    this.getQA();
  //  }, 100000);
  // $('textarea').autoResize();
 }
 
 closePopup() {
   $('.ballroomQuestionModal').modal('hide');
 }
 getQA(){
   let data = JSON.parse(localStorage.getItem('virtual'));
  //  this._fd.Liveanswers().subscribe((res=>{
  //    console.log(res);
  //    this.qaList = res.result;
  //  }))
 }
 postQuestion(value){
   let data = JSON.parse(localStorage.getItem('virtual'));
   let audi_id = '24';
  console.log(value, data.id, audi_id);
   this._fd.askLiveQuestions(data.id,value,audi_id).subscribe((res=>{
     //console.log(res);
     if(res.code == 1){
      this.toastr.success( 'Your question has been submitted');
     }
     this.textMessage.reset();
     $('.ballroomQuestionModal').modal('hide');
   }))
 }
 ngOnDestroy() {
   clearInterval(this.interval);
 }
}
