import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechTwentysixComponent } from './tech-twentysix.component';

describe('TechTwentysixComponent', () => {
  let component: TechTwentysixComponent;
  let fixture: ComponentFixture<TechTwentysixComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechTwentysixComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechTwentysixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
