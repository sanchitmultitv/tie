import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechThirtytwoComponent } from './tech-thirtytwo.component';

describe('TechThirtytwoComponent', () => {
  let component: TechThirtytwoComponent;
  let fixture: ComponentFixture<TechThirtytwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechThirtytwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechThirtytwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
