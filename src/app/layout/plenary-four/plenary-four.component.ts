import { Component, OnInit, OnDestroy } from '@angular/core';
import {FormControl} from '@angular/forms';
import { FetchDataService } from 'src/app/services/fetch-data.service';
declare var $: any;
@Component({
  selector: 'app-plenary-four',
  templateUrl: './plenary-four.component.html',
  styleUrls: ['./plenary-four.component.scss']
})
export class PlenaryFourComponent implements OnInit {
  videoPlayer = 'https://dhzjgfv9krlhb.cloudfront.net/vod/session24_28-NOV/playlist.m3u8';
  constructor(private _fd: FetchDataService) { }
  textMessage = new FormControl('');
  msg;
  qaList;
  interval;
  ngOnInit(): void {
    this.getQA();

  }
  closePopup() {
    $('.p4').modal('hide');
  } 
  getQA(){
    let data = JSON.parse(localStorage.getItem('virtual'));
   //  this._fd.Liveanswers().subscribe((res=>{
   //    console.log(res);
   //    this.qaList = res.result;
   //  }))
  }
  postQuestion(value){
    let data = JSON.parse(localStorage.getItem('virtual'));
    let audi_id = '55'
   console.log(value, data.id, audi_id);
    this._fd.askLiveQuestions(data.id,value,audi_id).subscribe((res=>{
      //console.log(res);
      if(res.code == 1){
        this.msg = 'Submitted Succesfully';
      }
      this.textMessage.reset();
      $('.p4').modal('toggle');
 
    }))
  }
  
  ngOnDestroy() {
    clearInterval(this.interval);
  }
}
