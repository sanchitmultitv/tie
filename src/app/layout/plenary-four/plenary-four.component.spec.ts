import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlenaryFourComponent } from './plenary-four.component';

describe('PlenaryFourComponent', () => {
  let component: PlenaryFourComponent;
  let fixture: ComponentFixture<PlenaryFourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlenaryFourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlenaryFourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
