import { Component, OnInit, OnDestroy } from '@angular/core';
import {FormControl} from '@angular/forms';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import { ChatService } from 'src/app/services/chat.service';
import {ToastrService} from 'ngx-toastr';
declare var $: any;
@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit, OnDestroy {
  textMessage = new FormControl('');
  msg;
  qaList;
  interval;
  exhibiton:any=[];
  documents:any=[];
  
  exhibition_id = '0';
  constructor(private _fd: FetchDataService, private chat: ChatService,private toastr:ToastrService) { }

  ngOnInit(): void {
   this.getQA();
   this.chat.getconnect('toujeo-139');
   let mydata = JSON.parse(localStorage.getItem('virtual'));
   this.chat.getMessages().subscribe((data=>{
      console.log('socketdata', data);
      let check = data.split('_');
      if(check[0]=='question' && check[1]=='reply' && check[2]== mydata.id){
        this.getQA();
      }
      // if(data == 'question_reply'){
      //   this.getQA();
      // }
    }));
    //  this.interval = setInterval(() => {
    //   this.getQA();
    // }, 5000);

  }
  closePopup() {
    $('.liveQuestionModal').modal('hide');
  }
  // getExhibit(){
  //   this._fd.getExhibition('Adapt').subscribe(res=>{
      
  //     this.exhibiton = res.result[0];
  //     this.documents = res.result[0].document;
  //     this.exhibition_id = res.result[0].id;
  //     console.log('exhibition',this.exhibiton); 
  //     //localStorage.setItem('exhibitData',JSON.stringify(res.result));
  //   });
  // }

  getQA(){
  //  console.log('exhibitonid',this.exhibition_id);
    let data = JSON.parse(localStorage.getItem('virtual'));
   // console.log('uid',data.id);
    this._fd.gethelpdeskanswers(data.id).subscribe((res=>{
      //console.log(res);
      this.qaList = res.result;
      // alert('hello');
    }));

  }
  postQuestion(value){
    console.log(value);
    if(value != '' && value != null){
      let data = JSON.parse(localStorage.getItem('virtual'));
      //  console.log(value, data.id);
      // this.getQA();
        // tslint:disable-next-line: align
        this._fd.helpdesk(data.id,value).subscribe((res=>{
          //console.log(res);
          // this.getQA();
          if(res.code == 1){
            this.textMessage.reset();
            let arr={
              "question":value,
              "answer":""
            };
            this.qaList.push(arr);
            //this.toastr.success( '!');
          // var d = $('.chat_message');
          // d.scrollTop(d.prop("scrollHeight"))
          }
           
        //  setTimeout(() => {
         //   $('#chat_messaged')[0].scrollTop = $('#chat_messaged')[0].scrollHeight-100;
          //  this.msg = '';
    //$('.liveQuestionModal').modal('hide');
      //    }, 2000);
          // setTimeout(() => {
          //   this.msg = '';
          //   $('.liveQuestionModal').modal('hide');
          // }, 2000);
         // this.textMessage.reset();
        }));
    }
  
    

  }
  ngOnDestroy() {
    clearInterval(this.interval);
  }
}
