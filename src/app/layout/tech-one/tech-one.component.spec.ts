import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechOneComponent } from './tech-one.component';

describe('TechOneComponent', () => {
  let component: TechOneComponent;
  let fixture: ComponentFixture<TechOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechOneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
