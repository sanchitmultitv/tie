import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-pavillion',
  templateUrl: './pavillion.component.html',
  styleUrls: ['./pavillion.component.scss']
})
export class PavillionComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  closePopup(){
    $('.pavillionModal').modal('hide');
  }
}
