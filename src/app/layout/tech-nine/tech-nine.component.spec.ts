import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechNineComponent } from './tech-nine.component';

describe('TechNineComponent', () => {
  let component: TechNineComponent;
  let fixture: ComponentFixture<TechNineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechNineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechNineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
