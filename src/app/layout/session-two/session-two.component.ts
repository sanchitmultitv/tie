import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FetchDataService } from 'src/app/services/fetch-data.service';
declare var $: any;
@Component({
  selector: 'app-session-two',
  templateUrl: './session-two.component.html',
  styleUrls: ['./session-two.component.scss']
})
export class SessionTwoComponent implements OnInit {
  going;
  getDesc : any;
  getHeading :any;
  getPort:any; getPort2:any; getPort3:any; getPort4:any;
  getPort5:any; getPort6:any; getPort7:any; getPort8:any;
  getPort9:any; getPort10:any; getPort11:any; getPort12:any;
  getPort13:any; getPort14:any; getPort15:any; getPort16:any;
  getPort17:any; getPort18:any; getPort19:any; getPort20:any;
  getPort21:any; getPort22:any; 
  constructor(private route: Router, private _fd:FetchDataService) { }

  ngOnInit(): void {
  }
  stepUpAnalytics(action) {
    let virtual: any = JSON.parse(localStorage.getItem('virtual'));
    let yyyy: any = new Date().getFullYear();
    let dd: any = new Date().getDate();
    let mm: any = new Date().getMonth() + 1;
    let time: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    if(virtual.company == ''){
      virtual.company = 'others';
    };
    const formData = new FormData();
    formData.append('event_id', virtual.event_id);
    formData.append('user_id', virtual.id);
    formData.append('name', virtual.name);
    formData.append('email', virtual.email);
    formData.append('company', virtual.company);
    formData.append('designation', virtual.designation);
    formData.append('action', action);
    formData.append('created', yyyy + '-' + mm + '-' + dd + ' ' + time);
    this._fd.analyticsPost(formData).subscribe(res => {
      console.log('asdf', res);
    });
  }
  showSessionPopup(value,desc,desc2,desc3,desc4,desc5,desc6,desc7,desc8,desc9,desc10,desc11,desc12,desc13,desc14,desc15,desc16,desc17,desc18,desc19,desc20,desc21,desc22,desc23,desc24) {
    this.getHeading = desc;
    this.getDesc = desc2;
    this.getPort = desc3;
    this.getPort2 = desc4;
    this.getPort3 = desc5;
    this.getPort4 = desc6;
    this.getPort5 = desc7;
    this.getPort6 = desc8;
    this.getPort7 = desc9;
    this.getPort8 = desc10;
    this.getPort9 = desc11;
    this.getPort10 = desc12;
    this.getPort11 = desc13;
    this.getPort12 = desc14;
    this.getPort13 = desc15;
    this.getPort14 = desc16;
    this.getPort15 = desc17;
    this.getPort16 =  desc18;
    this.getPort17 =  desc19;
    this.getPort18 =  desc20;
    this.getPort19 =  desc21;
    this.getPort20 =  desc22;
    this.getPort21 =  desc23;
    this.getPort22 =  desc24;
    this.going = value;
    $(".confirmModaltwo").modal('show');
  }
  closePopup() {
    $(".confirmModaltwo").modal('hide');
  }
  navigate() {
    this.route.navigate(['/tech-' + this.going]);
    $(".confirmModaltwo").modal('hide');
  }
}
