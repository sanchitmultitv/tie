import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RenovateThreeComponent } from './renovate-three.component';

describe('RenovateThreeComponent', () => {
  let component: RenovateThreeComponent;
  let fixture: ComponentFixture<RenovateThreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RenovateThreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RenovateThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
