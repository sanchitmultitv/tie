import { Component, OnInit } from '@angular/core';
import { Profiles } from '../../../app/layout/speakers/profile';
import { DomSanitizer } from '@angular/platform-browser';
declare var $: any;

@Component({
  selector: 'app-speakers',
  templateUrl: './speakers.component.html',
  styleUrls: ['./speakers.component.scss']
})
export class SpeakersComponent implements OnInit {

  constructor(private sanitiser: DomSanitizer) { }
  profiles: any = Profiles;
  document;
  documentName;
  speakerName;
  speakerContent;
  speakerPosition;
  profileObj:any = {};
  ngOnInit(): void {
  }
  getDocument() {
    $('.speakersDocument').modal('show');
    // window.open("https://s3.ap-south-1.amazonaws.com/acma.multitvsolution.in/assets/acme/speakers_profiles/Kenichi Ayukawa/Brief Profile - Mr. Kenichi Ayukawa.pdf","viewer"); 
    // this.document=this.sanitiser.bypassSecurityTrustResourceUrl(prof.file);
    // this.documentName = prof.name;
  }
  getDetails(profile: any) {
    this.profileObj = profile;
    $('.speakersDocument').modal('show');
  }
  closedocumentPopup() {
    $('.speakersDocument').modal('hide');

  }
}
