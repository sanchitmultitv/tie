import { Component, OnInit, ElementRef, ViewChild, Renderer2 } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {FormControl} from '@angular/forms';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import  * as html2canvas  from 'html2canvas';
import {ToastrService} from 'ngx-toastr';

import {NgbDateStruct, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';
import { ChatService } from 'src/app/services/chat.service';
declare var $: any;

@Component({
  selector: 'app-lifeboy',
  templateUrl: './lifeboy.component.html',
  styleUrls: ['./lifeboy.component.scss']
})
export class LifeboyComponent implements OnInit {
  textMessage = new FormControl('');
  msg;
  qaList;
  interval;
  pord_desc;
 datas:any;
 lhlhlh:any;
 hleft:any;
       nextB=1; 
       backB=221;
breif_flag = false;
exhibition_id = '0';
exhibiton: any = [];
brouchers: any = [];
videosource:any
graphic:any;
exhibitionName;
banner: 'https://virtualapi.multitvsolution.com/exhibitions/standard.jpeg';
product;
premiumList: any = [];

salesPerson: any = [];
model: NgbDateStruct;
date: {year: number, month: number};
newTimeSlots:any=[];
timeVal;
backButton=true;
nextButton=true;
boolTime= false;
isShow = true;
newMessage: string;
  msgs: string;
  newMSg =[];
  showDiv = true;
  showchat = true;
  hidechat = false;
  messageList = [];
  roomName: string;
  myname:any
  myId;
  uName;
  myid;
  exhibition_title;
  standardList:any
@ViewChild('screen') screen: ElementRef;
  @ViewChild('canvas') canvas: ElementRef;
  @ViewChild('downloadLink') downloadLink: ElementRef;
  constructor(private _fd: FetchDataService, private route: ActivatedRoute, private toastr: ToastrService,private calendar: NgbCalendar, private renderer: Renderer2, private chat: ChatService,private router:Router) { }

  ngOnInit(): void {
    this.premiumStalls();
    this.getExhibitionData();
    this.standardStalls();
    setTimeout(() => {
      this.route.paramMap.subscribe(params => {
        this.exhibitionName = params.get('exhibitName');
      let virtual: any = JSON.parse(localStorage.getItem('virtual'));
      this.myid = virtual.id;
    let yyyy: any = new Date().getFullYear();
    let dd: any = new Date().getDate();
    let mm: any = new Date().getMonth() + 1;
    let time: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    let action = 'exhibition_'+this.exhibitionName;
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    if(virtual.company == ''){
      virtual.company = 'others';
    };
    const formData = new FormData();
    formData.append('event_id', virtual.event_id);
    formData.append('user_id', virtual.id);
    formData.append('name', virtual.name);
    formData.append('email', virtual.email);
    formData.append('company', virtual.company);
    formData.append('designation', virtual.designation);
    formData.append('action', action);
    formData.append('created', yyyy + '-' + mm + '-' + dd + ' ' + time);
    this._fd.analyticsPost(formData).subscribe(res => {
      console.log('asdf', res);
    });
    });}, 3000);

    
    
    
    this.model = this.calendar.getToday();
    this.getQA();
    this.chat.getconnect('toujeo-139');
    this.chat.getMessages().subscribe((data=>{
     let check = data.split('_');
       if(check[0] == 'one2one' && check[1]== this.myid) {
        //alert(data);
        this.myname = check[2]
        this.toastr.success(this.myname+' Messaged You');
         this.getQA();
       }
     }));
    let myDate = this.model.year+'-'+this.model.month+'-'+this.model.day;
    //let myDate = '2020-10-25';
    let data = JSON.parse(localStorage.getItem('virtual'));
    this.myId = data.id;
    this._fd.totalTimeSlots(this.exhibitionName,myDate).subscribe(res=>{
       this.newTimeSlots=res.result;
     })
     //let myDate = this.model.year+'-'+this.model.month+'-'+this.model.day;
     
     let playVideo: any = document.getElementById('playVideo');
     window.onclick = (event) => {
       if (event.target == playVideo) {
         playVideo.style.display = "none";
         let pauseVideo: any = document.getElementById("video");
         pauseVideo.currentTime = 0;
         pauseVideo.pause();
       }
     }   
  }
  playShowVideo() {
    let playVideo: any = document.getElementById("video");
    playVideo.play();
    $('#playVideo').modal('show');
  }
  
  closeModalVideo(){
    let pauseVideo: any = document.getElementById("video");
    pauseVideo.currentTime = 0;
    pauseVideo.pause();
    $('#playVideo').modal('hide');
  }
  stepUpAnalytics(action) {
    let virtual: any = JSON.parse(localStorage.getItem('virtual'));
    let yyyy: any = new Date().getFullYear();
    let dd: any = new Date().getDate();
    let mm: any = new Date().getMonth() + 1;
    let time: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    if(virtual.company == ''){
      virtual.company = 'others';
    };
    const formData = new FormData();
    formData.append('event_id', virtual.event_id);
    formData.append('user_id', virtual.id);
    formData.append('name', virtual.name);
    formData.append('email', virtual.email);
    formData.append('company', virtual.company);
    formData.append('designation', virtual.designation);
    formData.append('action', action);
    formData.append('created', yyyy + '-' + mm + '-' + dd + ' ' + time);
    this._fd.analyticsPost(formData).subscribe(res => {
      console.log('asdf', res);
    });
  }
  getExhibitionData(){
    let data = JSON.parse(localStorage.getItem('virtual'));
    this.route.paramMap.subscribe(params => {
      this.exhibitionName = params.get('exhibitName');
      this._fd.getExhibition(this.exhibitionName, 'standard').subscribe(res => {
        //console.log('exhibition',res);
        this.exhibiton = res.result;
        this.brouchers = res.result[0].brochure;
        this.videosource = res.result[0].video;
        this.exhibition_title = res.result[0].title;
       // alert(this.exhibition_title);
      //this.graphic = res.result[0].graphics;
        this.banner = res.result[0].banner;
        this.salesPerson = res.result[0].sales;
       // this.product = res.result[0].product[0].url;
        //this.pord_desc =res.result[0].product[0].desc;
       // this.product = res.result[0].product[0].url
        console.log(this.banner, 'vauvduyvuduv')
      });
      // this._fd.getBrouchers(this.exhibitionName,data.id).subscribe(res=> {
      //   console.log(res.result);
      //   this.brouchers = res.result;
      // })
      
    });
   
  }
  sendMessage() {
    this.roomName = 'ddbjb1bk';
    if (this.msgs != '') {
      this.chat.sendMessage(this.msgs, this.uName, this.roomName);
      this.msgs = '';
    }
  }
  standardStalls() {
    this._fd.getstandarad().subscribe((res: any) => {
      // console.log(res);
      this.standardList = res.result;
      this.datas = this.standardList[0];
      console.log(this.standardList)
    });
  }
  
  next() {
    if(this.nextB>=221){
      console.log(this.nextB);
      this.nextB=1;
      this.nextButton=false
      this.backButton=true
     }else{
      
      this.nextButton=true
      this.backButton=true
      // this.backB=true
      this.hleft=  this.nextB++;
      // alert(this.hleft)
        // this.standardList[0].value
      this.lhlhlh =  this.standardList[this.hleft].id
      // console.log(this.lhlhlh)
      // alert(this.lhlhlh);
      this.router.navigate(['/exhibitionHall/lifeboy/' + this.lhlhlh]);
     }
     
 }

 previous() {
   if(this.backB<=0){
    this.backButton=false
    this.nextButton=true
    this.backB=221;
   }else{
     
    this.nextButton=true
    this.backButton=true
  this.hleft = this.backB--;
  // alert(this.hleft)
    // this.standardList[0].value
  this.lhlhlh =  this.standardList[this.hleft].id
  console.log(this.lhlhlh)
  // alert(this.lhlhlh);
  this.router.navigate(['/exhibitionHall/lifeboy/' + this.lhlhlh]);
   }
   
 }
 premiumStalls() {
  this._fd.getpremium().subscribe((res: any) => {
    this.premiumList = res.result;
  });
}
 lifeboy(title) {
  this.router.navigate(['/exhibitionHall/lifeboy', title]);
  $('.exDirectories').modal('hide');
}
premium(title) {
  this.router.navigate(['/exhibitionHall/premium', title]);
  $('.exDirectories').modal('hide');
}
closeStandard() {
  $('.exDirectories').modal('hide');
}
  hidediv() {
this.showDiv = false;
this.showchat = false;
this.hidechat = true;
  }
  show() {
    this.showDiv = true;
    this.hidechat = false;
this.showchat = true;
      }
      getUser() {
        let user = JSON.parse(localStorage.getItem('virtual'));
        this.uName = user.name;
      //  console.log(this.uName);
       // let room = JSON.parse(localStorage.getItem('room_id'));
        //console.log(room);
        this.roomName = 'ddbjb1bk';
        this.chat.addUser(this.uName, this.roomName);
      }
  getQA(){
    this.route.paramMap.subscribe(params => {
      this.exhibitionName = params.get('exhibitName');
    //  console.log('exhibitonid',this.exhibition_id);
      let data = JSON.parse(localStorage.getItem('virtual'));
     // console.log('uid',data.id);
      this._fd.getanswers(data.id,this.exhibitionName).subscribe((res=>{
        //console.log(res);
        this.qaList = res.result;
        // alert('hello');
      }))
    }); 
    }
    postQuestion(value) {
      this.route.paramMap.subscribe(params => {
        this.exhibitionName = params.get('exhibitName');
        let data = JSON.parse(localStorage.getItem('virtual'));
     console.log('value',value);
    // this.getQA();
    if(value != undefined){
        this._fd.askQuestions(data.id,data.name, value,this.exhibitionName,this.exhibiton[0].title).subscribe((res=>{
        if(res.code == 1) {
          this.msg = 'Submitted Succesfully';
        // var d = $('.chat_message');
        // d.scrollTop(d.prop("scrollHeight"))
        }
        // this.getQA();
        let arr = {
          'sender_id': data.id,
          'sender_name': data.name,
          'receiver_id':this.exhibitionName,
          'receiver_name':this.exhibition_title,
          'msg':value
        };
        this.qaList.push(arr);
         
        setTimeout(() => {
          $('#chat_messaged')[0].scrollTop = $('#chat_messaged')[0].scrollHeight-100;
          this.msg = '';
  //$('.liveQuestionModal').modal('hide');
        }, 2000);
        // setTimeout(() => {
        //   this.msg = '';
        //   $('.liveQuestionModal').modal('hide');
        // }, 2000);
        this.textMessage.reset();
      }))
    }
    });
  
    }
  changeProdcut(prod,descs){
    this.product = prod;
    this.pord_desc =descs;
  }
  closeproduct(){
    $('.productModal').modal('hide');
  }
  closeChat(){
    $('.liveQuestionModal').modal('hide');
  }
  closeright(){
    $('.rightGraphcModal').modal('hide');
  }
  closeLeft(){
    $('.leftGraphicModal').modal('hide');
  }
  cardPost(){
    let data = JSON.parse(localStorage.getItem('virtual'));

    const formsData = new FormData();
    formsData.append('user_id', data.id);
    formsData.append('exhibition_id',this.exhibiton[0].id );
    this._fd.postCard(formsData).subscribe((res:any)=>{
      console.log(res,'ressssd');
      if(res.code == 1){
        this.toastr.success( 'Card dropped successfully !!');
      }
      
    });
    //this.toastr.success( 'Card dropped successfully !!');
  }
  getTime(event: any, valClass, time) {
    // console.log(time);
    this.timeVal = time;
    const hasClass = event.target.classList.contains(valClass);
    $(".time-list li a.active").removeClass("active");
    // adding classname 'active' to current click li
    this.renderer.addClass(event.target, valClass);
    // if (hasClass) {
    //   //alert('has')
    //       this.renderer.removeClass(event.target, valClass);
    //     } else {
    //      // alert(valClass)
    //       this.renderer.addClass(event.target, valClass);
    //     }
      }
  // getExhibit(){
  //   this._fd.getExhibition().subscribe(res=>{
  //     console.log('exhibition',res);
  //     this.exhibiton = res.result;
  //     localStorage.setItem('exhibitData',JSON.stringify(res.result));
  //   });
  // }
  closePopup(){
$('.docsModal').modal('hide');
  }
  confirm() {
    // alert(this.dateModel);
 //console.log('val',dp);
 this.route.paramMap.subscribe(params => {
  this.exhibitionName = params.get('exhibitName');
  let data = JSON.parse(localStorage.getItem('virtual'));
  console.log(this.model.year+'-'+this.model.month+'-'+this.model.day+' '+this.timeVal);
  const CallData = new FormData();
  CallData.append('exhibition_id', this.exhibitionName);
  CallData.append('user_id', data.id);
  CallData.append('time', this.model.year+'-'+this.model.month+'-'+this.model.day+' '+this.timeVal);
  this._fd.schdeuleAcall(CallData).subscribe(res=>{
   console.log(res);
   if(res.code == 1){
    this.toastr.success( 'Call scheduled succesfully!');

    let myDate = this.model.year+'-'+this.model.month+'-'+this.model.day;
    //let myDate = '2020-10-25';
    let data = JSON.parse(localStorage.getItem('virtual'));
    this._fd.totalTimeSlots(this.exhibitionName,myDate).subscribe(res=>{
       console.log('timeresponse',res);
       this.newTimeSlots=res.result;
     })
    setTimeout(() => {
     $('.scheduleCallmodal').modal('hide');
    }, 2000);
   }
  
 });
 
});
   }
  onDateSelect(dates){
    console.log('aycycyt',dates.year+'-'+dates.month+'-'+dates.day);
    let myDate = dates.year+'-'+dates.month+'-'+dates.day;
    this.route.paramMap.subscribe(params => {
      this.exhibitionName = params.get('exhibitName');
      let data = JSON.parse(localStorage.getItem('virtual'));
      this._fd.totalTimeSlots(this.exhibitionName,myDate).subscribe(res=>{
        console.log('timeresponse',res);
        this.newTimeSlots=res.result;
      })
    });
    

  }
  closesales(){
    $('.salesModal').modal('hide');
      }
      closeCall(){
        $('.scheduleCallmodal').modal('hide');
      }
      openChat(){
        $('.liveQuestionModal').modal('show');
      }
      closethird(){
        $('.thirdGraphicModal').modal('hide');
      }
      closefour(){
        $('.fourthGraphcModal').modal('hide');
      }
      download(i, name){
        // html2canvas(this.screen.nativeElement).then(canvas => {
        //   this.canvas.nativeElement.src = canvas.toDataURL();
        //   this.downloadLink.nativeElement.href = canvas.toDataURL('image/png');
        //   this.downloadLink.nativeElement.download = name + '.png';
        //   this.downloadLink.nativeElement.click();
          
        // });
        // this.toastr.success( 'Card Dropped Succesfully!');
        var container = document.getElementById("hello" + i); //specific element on page
			//var container = document.body; // full page 
			// tslint:disable-next-line: indent
			// tslint:disable-next-line: align
			html2canvas(container).then(function(canvas) {
                var link = document.createElement("a");
                document.body.appendChild(link);
                link.download = name + ".png";
                link.href = canvas.toDataURL('image/png');
                link.target = '_blank';
                link.click();
            });
           // this.toastr.success( 'Card Dropped Succesfully!');
      }
      postBreifcase(title,url){
        let data = JSON.parse(localStorage.getItem('virtual'));
        const formData = new FormData();
        formData.append('event_id', '54');
        formData.append('user_id', data.id);
        formData.append('document_title', title);
        formData.append('document', url);
        this._fd.postBriefcase(formData).subscribe(res=>{
          console.log(res);
          this.toastr.success( 'Added in breifcase successfully !!');
          // this._fd.getBrouchers(this.exhibitionName,data.id).subscribe(res=> {
          //   console.log(res.result);
          //   this.brouchers = res.result;
          // })
         /// this.breif_flag = true;
        });
      }
      selectToday() {
        this.model = this.calendar.getToday();
        console.log(this.model);
      }
      
     
}
