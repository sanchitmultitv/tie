import { Component, OnInit, OnDestroy } from '@angular/core';
import { fadeAnimation } from '../../../../shared/animation/fade.animation';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { FetchDataService } from '../../../../services/fetch-data.service'
import { EventEmitter } from 'events';
import { ChatService } from '../../../../services/chat.service';
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-auditorium-one',
  templateUrl: './auditorium-one.component.html',
  styleUrls: ['../auditorimStl/audi.style.scss'],
  animations: [fadeAnimation],

})
export class AuditoriumOneComponent implements OnInit, OnDestroy {
  videoEnd = false;
 // videoPlayer = 'https://dhzjgfv9krlhb.cloudfront.net/abr/smil:stream3.smil/playlist.m3u8';
  // videoPlayer = '../assets/video/networking_lounge_video.mp4';
  videoPlayer = './assets/acma/video/Final_Trim_15_10_2020.mp4'
  constructor(private chatService: ChatService, private _fd: FetchDataService, private route: Router) { }
  getDesc : any;
  getHeading :any;
  getPort:any; getPort2:any; getPort3:any; getPort4:any;
  getPort5:any; getPort6:any; getPort7:any; getPort8:any;
  getPort9:any; getPort10:any; getPort11:any; getPort12:any;
  getPort13:any; getPort14:any; getPort15:any;
  
  ngOnInit(): void {
    console.log(this.videoPlayer, 'this is 1');
    $(document).ready(function(){
      $('[data-toggle="tooltip"]').tooltip();   
    });
  }
  videoEnded() {
    this.videoEnd = true;
  }
  playAudioClap() {
    let playaudio: any = document.getElementById('myAudioClap');
    playaudio.play();
  }
  stepUpAnalytics(action) {
    let virtual: any = JSON.parse(localStorage.getItem('virtual'));
    let yyyy: any = new Date().getFullYear();
    let dd: any = new Date().getDate();
    let mm: any = new Date().getMonth() + 1;
    let time: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    if(virtual.company == ''){
      virtual.company = 'others';
    };
    const formData = new FormData();
    formData.append('event_id', virtual.event_id);
    formData.append('user_id', virtual.id);
    formData.append('name', virtual.name);
    formData.append('email', virtual.email);
    formData.append('company', virtual.company);
    formData.append('designation', virtual.designation);
    formData.append('action', action);
    formData.append('created', yyyy + '-' + mm + '-' + dd + ' ' + time);
    this._fd.analyticsPost(formData).subscribe(res => {
      console.log('asdf', res);
    });
  }
  playAudioWhistle() {
    let playaudio: any = document.getElementById('myAudioWhistle');
    playaudio.play();
  }
  openGroupChat() {
    $('.groupchatOne').modal('show');
    this.messageList = [];
    this.loadData();
  }


  textMessage = new FormControl('');
  newMessage: string[] = [];
  msgs: string;
  messageList: any = [];
  roomName = 'serdia_1';
  serdia_room = localStorage.getItem('serdia_room');
  loadData() {
    this.chatGroup();
    this.chatService.getconnect('toujeo-52');
    let data = JSON.parse(localStorage.getItem('virtual'));
    this.chatService.addUser(data.name, this.serdia_room);
    localStorage.setItem('username', data.name);
    this.chatService.receiveMessages(this.serdia_room).subscribe((msgs: any) => {
      if (msgs.roomId === 1) {
        this.messageList.push(msgs);
      }
      console.log('demo', this.messageList);
    });
  }
  chatGroup() {
    this._fd.groupchating().subscribe(res => {
      console.log('groupChat', res);
      this.messageList = res.result;
    });
  }

  closePopup() {
    $('.groupchatOne').modal('hide');
  }
  postMessage(value) {
    let data = JSON.parse(localStorage.getItem('virtual'));
    this.chatService.sendMessage(value, data.name, this.serdia_room);
    this.textMessage.reset();
    // this.chatGroup();
    //this.newMessage.push(this.msgs);
  }
  going;
  showPlenaryPopup(value,desc,desc2,desc3,desc4,desc5,desc6,desc7,desc8,desc9,desc10,desc11,desc12,desc13,desc14,desc15,desc16) {
    // alert(ll)
    this.getHeading = desc;
    this.getDesc = desc2;
    this.getPort = desc3;
    this.getPort2 = desc4;
    this.getPort3 = desc5;
    this.getPort4 = desc6;
    this.getPort5 = desc7;
    this.getPort6 = desc8;
    this.getPort7 = desc9;
    this.getPort8 = desc10;
    this.getPort9 = desc11;
    this.getPort10 = desc12;
    this.getPort11 = desc13;
    this.getPort12 = desc14;
    this.getPort13 = desc15;
    this.getPort14 = desc16;
    // this.getPort15 = desc17;
    this.going = value;

    $(".plenaryModal").modal('show');
  }
  closePlenaryPopup() {
    $(".plenaryModal").modal('hide');
  }
  navigate() {
    this.route.navigate(['/plenary-' + this.going]);
    $(".plenaryModal").modal('hide');
  }
  ngOnDestroy() {
    // this.chatService.disconnect();
  }
}

