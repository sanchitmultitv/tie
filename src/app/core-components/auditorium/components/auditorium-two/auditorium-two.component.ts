import { ActivatedRoute } from '@angular/router';
import { Route } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ChatService } from 'src/app/services/chat.service';
import { FetchDataService } from 'src/app/services/fetch-data.service';
declare var $: any;

@Component({
  selector: 'app-auditorium-two',
  templateUrl: './auditorium-two.component.html',
  styleUrls: ['../auditorimStl/audi.style.scss']
})
export class AuditoriumTwoComponent implements OnInit {
  textMessage = new FormControl('');
  newMessage: string[] = [];
  msgs: string;
  isOrange = false;
  iscolor = true;
  messageList: any = [];
  roomName = 'serdia_2';
  serdia_room = localStorage.getItem('serdia_room');
  top;
  left;
  height;
  width;
  banner;
  videoEnd = false;
  id;
  msg;
  qaList;
  interval;
  videoPlayerTwo;
  poster;
  //videoPlayer = 'assets/reinvest/videoplayback.mp4';

  constructor(private chatService: ChatService, private _fd: FetchDataService, private route:ActivatedRoute) { }

  ngOnInit(): void {
   // this.getQA();
    // this.chatGroupTwo();
    this.getaudi();
    
    setTimeout(() => {
      this.stepUpAnalytics();
    }, 3000);
    this.interval= setInterval(() => {
      this.getHeartbeat(); 
         }, 60000);
  } 
  getHeartbeat(){
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
    });
    let data = JSON.parse(localStorage.getItem('virtual'));
    const formData = new FormData();
    formData.append('user_id', data.id );
    formData.append('event_id', '139');
    formData.append('audi', this.id);
    this._fd.heartbeat(formData).subscribe(res => {
      console.log(res);
    });
  }
  stepUpAnalytics() {
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
    });
    let virtual: any = JSON.parse(localStorage.getItem('virtual'));
    let yyyy: any = new Date().getFullYear();
    let dd: any = new Date().getDate();
    let mm: any = new Date().getMonth() + 1;
    let time: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    if(virtual.company == ''){
      virtual.company = 'others';
    };
    const formData = new FormData();
    formData.append('event_id', virtual.event_id);
    formData.append('user_id', virtual.id);
    formData.append('name', virtual.name);
    formData.append('email', virtual.email);
    formData.append('company', virtual.company);
    formData.append('designation', virtual.designation);
    formData.append('action', 'auditorium_'+this.id);
    formData.append('created', yyyy + '-' + mm + '-' + dd + ' ' + time);
    this._fd.analyticsPost(formData).subscribe(res => {
      console.log('asdf', res);
    });
  }
  closePopups() {
    $('.t16').modal('hide');
  } 
  getQA(){
    let data = JSON.parse(localStorage.getItem('virtual'));
   //  this._fd.Liveanswers().subscribe((res=>{
   //    console.log(res);
   //    this.qaList = res.result;
   //  }))
  }
  postQuestion(value){
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
    });
    let data = JSON.parse(localStorage.getItem('virtual'));
    let audi_id = this.id;
   console.log(value, data.id, audi_id);
    this._fd.askLiveQuestions(data.id,value,audi_id).subscribe((res=>{
      //console.log(res);
      if(res.code == 1){
        this.msg = 'Submitted Succesfully';
      }
      this.textMessage.reset();
      $('.t16').modal('toggle');
 
    }))
  }
  ngOnDestroy() {
    clearInterval(this.interval);
  }
  videoEnded() {
    this.videoEnd = true;
  }
  getaudi(){
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
    });
    this._fd.activeAudi(this.id).subscribe(res=>{
      console.log('res',res);
      this.videoPlayerTwo = res.result[0].stream;
      this.top = res.result[0].player_top;
      this.left = res.result[0].player_left;
      this.width = res.result[0].player_width;
      this.height = res.result[0].player_height;
      this.banner = res.result[0].banner;
      this.poster = res.result[0].poster;
    });
  }
  playAudioClap() {
    let playaudio: any = document.getElementById('myAudioClap');
    playaudio.play();
  }
  playAudioWhistle() {
    let playaudio: any = document.getElementById('myAudioWhistle');
    playaudio.play();
  }
  openGroupChat() {
    $('.groupchatTwo').modal('show')
    this.loadData();
  }
  changeStream(){
this.isOrange = false;
this.iscolor = true;
  }
  changeStreamenglish(){
this.iscolor = false;
this.isOrange = true;
  }

  loadData(): void {
    this.chatGroupTwo();
    this.chatService.getconnect('toujeo-52');
    // this.chatService.getMessages().subscribe((data => {
    //   if (data == 'group_chat') {
    //     this.chatGroupTwo();
    //   }
    // }));
    let data = JSON.parse(localStorage.getItem('virtual'));
    this.chatService.addUser(data.name, this.serdia_room);
    localStorage.setItem('username', data.name);
    this.chatService
      .receiveMessages(this.serdia_room)
      .subscribe((msgs: any) => {
        if (msgs.roomId === 2){
          this.messageList.push(msgs);
        }        
        console.log('demo', this.messageList);
      });
  }
  chatGroupTwo() {
    this._fd.groupchatingtwo().subscribe(res => {
      console.log('res', res);
      this.messageList = res.result;
      // $('.groupchatsModal').modal('toggle');
      // $('.groupchatsModal').trigger('click');

    });
  }
  closePopup() {
    $('.groupchatTwo').modal('hide');
  }
  postMessageTwo(value) {
    // console.log(value);
    let data = JSON.parse(localStorage.getItem('virtual'));
    // console.log(data.name);
    this.chatService.sendMessage(value, data.name, this.serdia_room);
    // console.log(this.roomName);
    this.textMessage.reset();
    // this.chatGroupTwo();
    //this.newMessage.push(this.msgs);
  }
}
