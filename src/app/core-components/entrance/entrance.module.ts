import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EntranceRoutingModule } from './entrance-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    EntranceRoutingModule
  ]
})
export class EntranceModule { }
