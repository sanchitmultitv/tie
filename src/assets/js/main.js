let video;
let pg;
let lastSnapShot;

let timer = 0;
let showLatestPhoto = false;

let backgroundScore;
//let font;
var camerabutton;
var resetbutton;
var downloadbutton;
let img;
let cWidth = 800;
let cHeight = 600;

function preload () {
  //  backgroundScore = loadSound('Assets/backgroundScore.mp3');
  //font = loadFont('CourierNewBold.ttf');
}

function setup () {
  createCanvas (windowWidth, windowHeight);

  camerabutton = createImg ('');
  camerabutton.mousePressed (takePicture);
  resetbutton = createImg ('PhotoBooth_files/retake.png');
  resetbutton.mousePressed (resetcamera);
  downloadbutton = createImg ('PhotoBooth_files/download.png');
  downloadbutton.mousePressed (downloadPicture);
  resetbutton.hide ();
  downloadbutton.hide ();

  video = createCapture (VIDEO);
  video.size (cWidth, cHeight);
  video.hide ();

  //backgroundScore.loop();
  img = loadImage ('PhotoBooth_files/logo.png');
  pg = createGraphics (cWidth + 40, cHeight + 40);
  background (0);
  for (var i = 0; i < 200; i++) {
    var diameter = random (10, 5);
    noStroke ();
    fill (248, 201, 48);
    ellipse (random (width), random (height), diameter, diameter);
  }
}

function draw () {
  x = (windowWidth - cWidth) / 2;
  //y= ((windowHeight-cHeight)/2)-120;
  y = 40;

  camerabutton.position (windowWidth / 2 - 50, y + cHeight + 40);
  resetbutton.position (windowWidth / 2 + 150, y + cHeight + 40);
  downloadbutton.position (windowWidth / 2 - 250, y + cHeight + 40);

  fill (32, 55, 96);
  rect (x - 20, y - 20, 40 + cWidth, 40 + cHeight);

  push ();
  translate (width, 0);
  scale (-1, 1);
  image (video, x, y, 800, 600);
  pop ();

  //image(video,x,y,cWidth, cHeight);
  image (img, x + 20, y);

  //Show the last image taken for a short period
  if (showLatestPhoto) {
    image (pg, x - 20, y - 20);
  }
}

function resetcamera () {
  showLatestPhoto = false;
  camerabutton.show ();
  resetbutton.hide ();
  downloadbutton.hide ();
}

function takePicture () {
  camerabutton.hide ();
  resetbutton.show ();
  downloadbutton.show ();
  pg = get (x - 20, y - 20, 40 + cWidth, 40 + cHeight);
  showLatestPhoto = true;
}
function downloadPicture () {
  //let to_save = get( x, y, 800, 600 ); // Grab an image of a 100x200 rectangle at (20,30).
  pg.save ('MyPicture.png');
  showLatestPhoto = false;
  camerabutton.show ();
  resetbutton.hide ();
  downloadbutton.hide ();
  //to_save.save("MyPicture.png");
}
function windowResized () {
  resizeCanvas (windowWidth, windowHeight);
  background (0);
  for (var i = 0; i < 200; i++) {
    var diameter = random (2, 5);
    noStroke ();
    fill (255);
    ellipse (random (width), random (height), diameter, diameter);
  }
}
